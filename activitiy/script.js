//console.log("Hello World")

// Number One Requirement
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json());
.then((json) => {json.map(posts => console.log(posts.title))});
.then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts/10", {
		// HTTP Method
		method:"GET",
		// Specifies the content that it will pass in JSON format
		headers: {
			"Content-Type":"application/json"
		},
		// Sets the content/body data of the "Request" object to be sent to the backend/server
		body: JSON.stringify({
			title: " ",
		})

	})
	.then((response) => response.json())
	.then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts", {
		// HTTP Method
		method:"POST",
		// Specifies the content that it will pass in JSON format
		headers: {
			"Content-Type":"application/json"
		},
		// Sets the content/body data of the "Request" object to be sent to the backend/server
		body: JSON.stringify({
			title: "New Post",
			body: "To Do List",
			userId: 1
		})

	})
	.then((response) => response.json())
	.then((json) => console.log(json))

	fetch("https://jsonplaceholder.typicode.com/posts/10", {
		// HTTP Method
		method:"PUT",
		// Specifies the content that it will pass in JSON format
		headers: {
			"Content-Type":"application/json"
		},
		// Sets the content/body data of the "Request" object to be sent to the backend/server
		body: JSON.stringify({
			title: "Update Post",
			body: "Update To Do List",
			userId: 1
		})

	})

	// response of the server based on the request.
	.then((response) => response.json())
	.then((json) => console.log(json))

	fetch("https://jsonplaceholder.typicode.com/posts/10", {
		// HTTP Method
		method:"PATCHS",
		// Specifies the content that it will pass in JSON format
		headers: {
			"Content-Type":"application/json"
		},
		// Sets the content/body data of the "Request" object to be sent to the backend/server
		body: JSON.stringify({
			// title: "Corrected Post",
			title: "New Title",
			body: " This is a body",
			completed: "true",
			Date_Completed: "August 27, 2022",
			userId: 1
		})

	})

	// response of the server based on the request.
	.then((response) => response.json())
	.then((json) => console.log(json))

	// Deleting a post
	// (delete, /posts/:id, DELETE)
	fetch("https://jsonplaceholder.typicode.com/posts/10", {
	method: "DELETE"
	})

